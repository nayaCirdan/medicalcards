class Modal {
    constructor(id, title) {
        this._id=id;
        this._title=title;
    }


    createModal(){
        const modalContainer=document.createElement('div');
        modalContainer.classList.add('modal');
        modalContainer.setAttribute('id', `${this._id}`);
        modalContainer.setAttribute('tabindex', '-1');
        modalContainer.setAttribute('role', 'dialog');
        modalContainer.setAttribute('aria-labelledby', `${this._id}Label`);
        modalContainer.setAttribute('aria-hidden', 'true');


        modalContainer.innerHTML=`<div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-cards">
                    <h5 class="modal-title" id=${this._id}>${this._title}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body d-flex">
               
            </div>
        </div>
    </div>`;
        document.body.append(modalContainer);
    }
}