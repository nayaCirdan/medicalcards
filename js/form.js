class CreatItemForForm {

    constructor(element = "input", type = "text", name = "", parent, placeholder = '', options = []) {
        this.element = element;
        this.type = type;
        this.name = name;
        this.parent = parent;
        this.options = options;
        this.placeholder = placeholder;
        this.component = null;
        this.creatItem();
        this.render();
    }

    creatItem() {

        if (this.element === 'input') {
            this.component = document.createElement('input');
            this.component.type = this.type;
            this.component.name = this.name;
            this.component.placeholder = this.placeholder;

        } else if (this.element === 'select') {
            this.component = document.createElement('select');
            this.component.name = this.name;
            this.options.forEach(element => {
                const option12 = document.createElement('option');
                option12.value = element;
                option12.innerText = element;
                this.component.add(option12);
            });
        } else if (this.element === 'textarea') {
            this.component = document.createElement('textarea');
            this.component.name = this.name;
            this.component.placeholder = this.placeholder;
        }
    }

    render() {
        this.parent.append(this.component);
    }
}



class Form {
    constructor(name) {
        this._name = name;

        this._form = document.createElement('form');

        this._form.setAttribute('name', this._name);


    }

    async createForm(formType, targetID) {
        let renderElement = this._form;

        switch (formType) {
            case 'login':
                const loginInput = new CreatItemForForm('input', 'text', 'userLogin', renderElement, 'Login');
                const passwordInput = new CreatItemForForm('input', 'password', 'userPassword', renderElement, 'Password');
                const btnLogin = new CreatItemForForm('input', 'submit', 'loginBtn', renderElement);
                break;

            case 'create-card':

                let cardSelect = new CreatItemForForm('select', '', 'doctorType', renderElement, '', ["No Doctor", "Cardiologist", "Therapist", "Dentist"]);
                const formContainer = document.createElement('div');
                this._form.append(formContainer);
                renderElement = formContainer;
                let submitHandler = null;

                cardSelect.component.addEventListener('change', (e) => {
                    if(submitHandler){
                        this._form.removeEventListener('submit', submitHandler);
                    }
                    renderElement.innerHTML = '';
                    if (e.target.value === 'Therapist') {
                        const md = e.target.value;
                        const name = new CreatItemForForm('input', 'text', 'Name', renderElement, 'Your first name and last name');
                        const age = new CreatItemForForm('input', 'number', 'Age', renderElement, 'Age');
                        const visitPurpose = new CreatItemForForm('input', 'text', 'Visit_Purpose', renderElement, 'The purpose of the visit');
                        const briefDescription = new CreatItemForForm('textarea', '', 'Brief_Description', renderElement, 'Description');
                        const priority = new CreatItemForForm('select', '', 'Priority', renderElement, '', ["high", "normal", "low"]);
                        const btnCreate = new CreatItemForForm('input', 'submit', 'btnCreate', renderElement);

                        submitHandler = async (e) => {
                            e.preventDefault();
                            const newVisit = new VisitTherapist(name.component.value, visitPurpose.component.value, briefDescription.component.value, md, priority.component.value, age.component.value);
                            console.log(newVisit);
                            $("#createCardModal").modal('hide');
                        };

                        this._form.addEventListener('submit', submitHandler);

                    } else if (e.target.value === 'Dentist') {
                        const md = e.target.value;
                        const name = new CreatItemForForm('input', 'text', 'Name', renderElement, 'Your first name and last name');
                        const visitPurpose = new CreatItemForForm('input', 'text', 'Visit_Purpose', renderElement, 'The purpose of the visit');
                        const briefDescription = new CreatItemForForm('textarea', '', 'Brief_Description', renderElement, 'Description');
                        const priority = new CreatItemForForm('select', '', 'Priority', renderElement, '', ["high", "normal", "low"]);
                        const dateLabel = document.createElement('label');
                        dateLabel.innerText = 'Date of last visit:';
                        renderElement.append(dateLabel);
                        const lastVisit = new CreatItemForForm('input', 'date', 'lastVisit', renderElement);
                        const btnCreate = new CreatItemForForm('input', 'submit', 'btnCreate', renderElement);

                        submitHandler = async (e) => {
                            e.preventDefault();
                            const newVisit = new VisitDentist(name.component.value, visitPurpose.component.value, briefDescription.component.value, md, priority.component.value, lastVisit.component.value);
                            console.log(newVisit);
                            $("#createCardModal").modal('hide');
                        }

                        this._form.addEventListener('submit', submitHandler);

                    } else if (e.target.value === 'Cardiologist') {
                        const md = e.target.value;
                        const name = new CreatItemForForm('input', 'text', 'Name', renderElement, 'Your first name and last name');
                        const age = new CreatItemForForm('input', 'number', 'Age', renderElement, 'Age');
                        const normalBloodPressure = new CreatItemForForm('input', 'text', 'BP ', renderElement, 'Blood Pressure');
                        const bodyMassIndex = new CreatItemForForm('input', 'text', 'BMI ', renderElement, 'Body Mass Index');
                        const cardioVascularIlnesses = new CreatItemForForm('input', 'text', 'Cardio_Vascular_Illnesses', renderElement, 'Cardio Vascular Illnesses');
                        const visitPurpose = new CreatItemForForm('input', 'text', 'Visit_Purpose', renderElement, 'The purpose of the visit');
                        const briefDescription = new CreatItemForForm('textarea', '', 'Brief_Description', renderElement, 'Description');
                        const priority = new CreatItemForForm('select', '', 'Priority', renderElement, '', ["high", "normal", "low"]);
                        const btnCreate = new CreatItemForForm('input', 'submit', 'btnCreate', renderElement);

                        submitHandler = async (e) => {
                            e.preventDefault();
                            const newVisit = new VisitCardiolodist(name.component.value, visitPurpose.component.value, briefDescription.component.value, md, priority.component.value, normalBloodPressure.component.value, bodyMassIndex.component.value, cardioVascularIlnesses.component.value, age.component.value);
                            console.log(newVisit);
                            $("#createCardModal").modal('hide');
                        }

                        this._form.addEventListener('submit', submitHandler);
                    }
                });
                break;

            case 'edit-card':
                const requiredCard = await board.updateCards(`http://cards.danit.com.ua/cards/${targetID}`);
                console.log(requiredCard);
                let selectVisit = new CreatItemForForm('select', '', 'doctorType', renderElement, '', ["No Doctor", "Cardiologist", "Therapist", "Dentist"]);
                selectVisit.component.value = requiredCard.MD;
                const formWrapper = document.createElement('div');
                this._form.append(formWrapper);
                renderElement = formWrapper;

                if (requiredCard.MD === 'Therapist') {
                    const name = new CreatItemForForm('input', 'text', 'Name', renderElement, 'Your first name and last name');
                    name.component.value = requiredCard.Name;
                    const age = new CreatItemForForm('input', 'number', 'Age', renderElement, 'Age');
                    age.component.value = requiredCard.Age;
                    const visitPurpose = new CreatItemForForm('input', 'text', 'Visit_Purpose', renderElement, 'The purpose of the visit');
                    visitPurpose.component.value = requiredCard.Purpose;
                    const briefDescription = new CreatItemForForm('textarea', '', 'Brief_Description', renderElement, 'Description');
                    briefDescription.component.value = requiredCard.Brief_Description;
                    const priority = new CreatItemForForm('select', '', 'Priority', renderElement, '', ["high", "normal", "low"]);
                    priority.component.value = requiredCard.Priority;
                    const btnCreate = new CreatItemForForm('input', 'submit', 'btnCreate', renderElement);
                    console.log(requiredCard);

                    this._form.addEventListener('submit', async (e) => {
                        e.preventDefault();
                        const updatedVisit = new VisitTherapist(name.component.value, visitPurpose.component.value, briefDescription.component.value, selectVisit.component.value, priority.component.value, age.component.value, requiredCard.id);
                        console.log("Updated visit - ", updatedVisit);
                        $("#editCardModal").modal('hide');

                    });

                } else if (requiredCard.MD === 'Dentist') {
                    const name = new CreatItemForForm('input', 'text', 'Name', renderElement, 'Your first name and last name');
                    name.component.value = requiredCard.Name;
                    const visitPurpose = new CreatItemForForm('input', 'text', 'Visit_Purpose', renderElement, 'The purpose of the visit');
                    visitPurpose.component.value = requiredCard.Purpose;
                    const briefDescription = new CreatItemForForm('textarea', '', 'Brief_Description', renderElement, 'Description');
                    briefDescription.component.value = requiredCard.Brief_Description;
                    const priority = new CreatItemForForm('select', '', 'Priority', renderElement, '', ["high", "normal", "low"]);
                    priority.component.value = requiredCard.Priority;
                    const dateLabel = document.createElement('label');
                    dateLabel.innerText = 'Date of last visit:';
                    renderElement.append(dateLabel);
                    const lastVisit = new CreatItemForForm('input', 'date', 'lastVisit', renderElement);
                    lastVisit.component.value = requiredCard.Last_Visit;
                    const btnCreate = new CreatItemForForm('input', 'submit', 'btnCreate', renderElement);

                    this._form.addEventListener('submit', async (e) => {
                        e.preventDefault();
                        const updatedVisit = new VisitDentist(name.component.value, visitPurpose.component.value, briefDescription.component.value, selectVisit.component.value, priority.component.value, lastVisit.component.value, requiredCard.id);
                        console.log("Updated visit - ", updatedVisit);
                        $("#editCardModal").modal('hide');

                    });
                } else if (requiredCard.MD === 'Cardiologist') {
                    const name = new CreatItemForForm('input', 'text', 'Name', renderElement, 'Your first name and last name');
                    name.component.value = requiredCard.Name;
                    const age = new CreatItemForForm('input', 'number', 'Age', renderElement, 'Age');
                    age.component.value = requiredCard.Age;
                    const normalBloodPressure = new CreatItemForForm('input', 'text', 'BP ', renderElement, 'Blood Pressure');
                    normalBloodPressure.component.value = requiredCard.BP;
                    const bodyMassIndex = new CreatItemForForm('input', 'text', 'BMI ', renderElement, 'Body Mass Index');
                    bodyMassIndex.component.value = requiredCard.BMI;
                    const cardioVascularIlnesses = new CreatItemForForm('input', 'text', 'Cardio_Vascular_Illnesses', renderElement, 'Cardio Vascular Illnesses');
                    cardioVascularIlnesses.component.value = requiredCard.Ilnesses;
                    const visitPurpose = new CreatItemForForm('input', 'text', 'Visit_Purpose', renderElement, 'The purpose of the visit');
                    visitPurpose.component.value = requiredCard.Purpose;
                    const briefDescription = new CreatItemForForm('textarea', '', 'Brief_Description', renderElement, 'Description');
                    briefDescription.component.value = requiredCard.Brief_Description;
                    const priority = new CreatItemForForm('select', '', 'Priority', renderElement, '', ["high", "normal", "low"]);
                    priority.component.value = requiredCard.Priority;
                    const btnCreate = new CreatItemForForm('input', 'submit', 'btnCreate', renderElement);

                    this._form.addEventListener('submit', async (e) => {
                        e.preventDefault();
                        const updatedVisit = new VisitCardiolodist(name.component.value, visitPurpose.component.value, briefDescription.component.value, selectVisit.component.value, priority.component.value, normalBloodPressure.component.value, bodyMassIndex.component.value, cardioVascularIlnesses.component.value, age.component.value, requiredCard.id);
                        console.log("Updated visit - ", updatedVisit);
                        $("#editCardModal").modal('hide');

                    });
                }
                break;

            case 'delete-card':
                const infoMessage = document.createElement('p');
                infoMessage.innerText = 'Are you sure, you want to delete this card? Enter your password to confirm';
                this._form.append(infoMessage);
                let passwordInputDelete = new CreatItemForForm('input', 'password', 'userPassword', renderElement);
                let btnDelete = new CreatItemForForm('input', 'submit', 'deleteCardBtn', renderElement);
                passwordInputDelete.component.focus();
                console.log('This is ID of Button', targetID);
                this._form.addEventListener('submit', (event) => {
                    event.preventDefault();
                    deleteSelectedForm(this._form, targetID);
                })
                break;

            case 'filter':
                const searchByTitle = new CreatItemForForm('input', 'text', 'searchByTitle', renderElement, 'Search by Title');
                const selectPriority = new CreatItemForForm('select', '', 'selectPriority', renderElement, '', ["all", "high", "normal", "low"]);
                // const selectStatus = new CreatItemForForm('select', '', 'selectStatus', renderElement, '', ["open", "done"]);
                const filterBtn = new CreatItemForForm('input', 'submit', 'filterBtn', renderElement);
                this._form.classList = "d-flex flex-column flex-sm-row align-items-center justify-content-around filter-form";

                renderElement.addEventListener('submit', (e) => {
                    e.preventDefault();
                    let filterPriority = selectPriority.component.value;
                    let searchTitle = searchByTitle.component.value;
                    console.log(filterPriority);
                    console.log(searchTitle);
                    let helperFilterArray = [];

                    function findCard(card) {
                        if (filterPriority === 'all') {
                            if (card["Name"].includes(searchTitle)) {
                                return true;
                            }
                        } else {
                            if (card["Priority"] === filterPriority && card["Name"].includes(searchTitle)) {
                                return true;
                            }
                        }
                    }

                    function renderFilteredCards() {
                        console.log(board.feilteredCards);
                        const cardRow = document.querySelector('.card-row');
                        cardRow.innerHTML = '';
                        board.renderCards(cardRow, helperFilterArray);
                    }

                    async function getAllCard() {
                        let allCardsOnFilterSubmit = await board.updateCards('http://cards.danit.com.ua/cards');
                        console.log(allCardsOnFilterSubmit);
                        if (searchTitle === "") {
                            helperFilterArray = allCardsOnFilterSubmit.filter((card) => {
                                if (filterPriority === 'all') {
                                    return card;
                                } else {
                                    return card["Priority"] === filterPriority;
                                }
                            });
                        } else {
                            helperFilterArray = allCardsOnFilterSubmit.filter(findCard);
                        }
                        renderFilteredCards();

                    }

                    getAllCard();


                });
                break;

        }
    }
    renderForm(where) {
        where.innerHTML = '';
        where.append(this._form);
    }
}

function deleteSelectedForm(form, id) {
    const passwordValue = form.querySelector('input[name="userPassword"]').value;
    const checkPassword = axios.post('http://cards.danit.com.ua/login', {
        "email": `${window.loginValue}`,
        "password": `${passwordValue}`
    }, {
        headers: {
            "Content-Type": "application/json"
        }
    }).then(response => {
        if (response.data.token) {
            console.log("Coorrect password. Recieved token: ", response.data.token);

            const deleteCard = axios.delete(`http://cards.danit.com.ua/cards/${id}`, {
                headers: {
                    "Authorization": `Bearer ${board.token}`
                }
            }).then(deleteResponse => {
                console.log(deleteResponse.data);
                if (deleteResponse.data.status === "Success") {
                    $("#deleteCardModal").modal('hide');
                    let cardBoard = document.querySelector('.card-row');
                    board.renderCards(cardBoard);
                }
                else {
                    const target = document.querySelector('input[name="deleteCardBtn"]');
                    if (document.querySelector('.error-message')) {
                        document.querySelector('.error-message').remove();
                    }
                    target.insertAdjacentHTML('beforebegin', '<span class="error-message">Network error. Try again later.</span>');
                }
            })

        } else {
            const target = document.querySelector('input[name="deleteCardBtn"]');
            if (document.querySelector('.error-message')) {
                document.querySelector('.error-message').remove();
            }
            target.insertAdjacentHTML('beforebegin', '<span class="error-message">Invalid password. Try again.</span>');
        }
    });
}

