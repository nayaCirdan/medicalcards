

const board = new MainBoard('./img/logo2.png', target);

const btnLogin = document.querySelector('.header-btn');
btnLogin.classList.add('btn-login');
btnLogin.setAttribute('data-name', 'login');
btnLogin.setAttribute('data-target', '#loginModal');
btnLogin.setAttribute('data-toggle', 'modal');
let modalId = null;

function getModalId(btn) {
    let btnDataTarget = btn.dataset.target;
    modalId = btnDataTarget.replace('#', '');
    return modalId;
}

btnLogin.addEventListener('click', () => {
    let lastModal = document.querySelector('.modal');

    if (lastModal) {
        lastModal.remove();
    }
    getModalId(btnLogin);

    let whatType = btnLogin.dataset.name;
    const loginModal = new Modal(modalId, 'Login');
    loginModal.createModal();
    let innerForm = new Form(whatType);
    innerForm.createForm(whatType);
    innerForm.renderForm(document.querySelector('.modal-body'));
    console.log(innerForm._form);
    innerForm._form.addEventListener('submit', (event) => {
        event.preventDefault();
        window.loginValue = document.querySelector('input[name="userLogin"]').value;
        const passwordValue = document.querySelector('input[name="userPassword"]').value;
        const loginRequest = axios.post('http://cards.danit.com.ua/login', {
            "email": `${window.loginValue}`,
            "password": `${passwordValue}`
        }, {
            headers: {
                "Content-Type": "application/json"
            }
        }).then(response => {
            if (response.data.token) {
                board.token = response.data.token;
                document.cookie = `${response.data.token}; max-age = 3600`
                board.renderCards(document.querySelector('.card-row'));
                createFilterForm();
                addCreateCardBtn(btnLogin);
                btnLogin.remove();
                $("#loginModal").modal('hide');
            } else {
                const target = document.querySelector('input[name="loginBtn"]');
                if(document.querySelector('.error-message')){
                document.querySelector('.error-message').remove();
                }
                target.insertAdjacentHTML('beforebegin', '<span class="error-message">Invalid e-mail/password. Try again.</span>')
            }
        });
    })
});

function createFilterForm() {
    const filterForm = new Form('filter');
    filterForm.createForm('filter');
    filterForm.renderForm(document.querySelector('.filter-row'));
}

function addCreateCardBtn(target) {
    target.insertAdjacentHTML('afterend', `<button type="button" class="btn btn-light btn-create" data-name="create-card" data-target="#createCardModal" data-toggle="modal">Create</button>`);
    const btnCreateCard = document.querySelector('.btn-create');

    btnCreateCard.addEventListener('click', () => {

        let lastModal = document.querySelector('.modal');

        if (lastModal) {
            lastModal.remove();
        }

        getModalId(btnCreateCard);

        let whatType = btnCreateCard.dataset.name;
        const createCardModal = new Modal(modalId, 'Create Card');
        createCardModal.createModal();
        let innerForm = new Form(whatType);
        innerForm.createForm(whatType);
        innerForm.renderForm(document.querySelector('.modal-body'));
    });
}