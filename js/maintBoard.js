class MainBoard {
    constructor(logo, parent) {
        this.token = null;
        this.parent = parent;
        this.logo = logo;
        this.feilteredCards = [];
        this.renderOnload(this.parent);
    }

    renderOnload(parent) {
        parent.innerHTML = `<header class="header bg-primary">
        <div class="container">
            <div class="row justify-content-between align-items-center header-row">
                <img src="${this.logo}" alt="logo" class="logo">
                <button type="button" class="btn btn-light header-btn">Login</button>
                
            </div>
        </div>
    </header>

    <section class="board-filter">
        <div class="container">
            <div class="row justify-content-between align-items-center filter-row">
                
            </div>
        </div>
    </section>

    <section class="card-board" id="cards-container">
        <div class="container">
            <div class="row justify-content-around align-items-start card-row flex-wrap">
        
            </div>
        </div>
    </section>`;

    }

    async renderCards(parent, cardsArray) {
        parent.innerHTML = '';
        let cards = null;
        if (cardsArray) {
            cards = cardsArray;
        }
        else {
            cards = await this.updateCards('http://cards.danit.com.ua/cards');
        }
        if (cards.length>0) {
            cards.forEach(element => {
                const card = document.createElement('div');
                $(card).draggable({containment: "#cards-container" ,stack: card });

                card.setAttribute('draggable', 'true');
                card.setAttribute('id', `cardContainer-${element.id}`);
                card.classList = 'onpage-card d-flex flex-column justify-content-around align-items-center';
                card.innerHTML = `
                <i class="fa fa-user-md font-icon text-primary"></i>
                <h2 class="text-primary">${element.Name}</h2>
                <h2 class="text-dark">${element.MD}</h2>

                <div class="collapse card-wrapper-style" id="collapseExample${element.id}">
                    <div class="card card-body own-card-style" id="hiddenProps${element.id}">
                    </div>
                </div>


                <div class="card-button-container d-flex justify-content-between">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton${element.id}" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Edit
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton${element.id}">
                            <a class="dropdown-item btn-edit" id="edit-card-btn${element.id}" href="#">Edit</a>
                            <a class="dropdown-item btn-remove" id="remove-card-btn${element.id}" href="#">Remove</a>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#collapseExample${element.id}" aria-expanded="false" aria-controls="collapseExample${element.id}">Show more</button>
                </div>
                
                `;
                parent.append(card);

                Object.keys(element).forEach(elem => {
                    if(elem!=="Name"&&elem!=="MD"&&elem!=="id"){
                        const text = `<h3 class="text-secondary"><span class="text-body">${elem}:</span> ${element[elem]}</h3>`;
                        document.querySelector(`#hiddenProps${element.id}`).insertAdjacentHTML('beforeend', text);
                    }
                });


                const btnEditCard = card.querySelector('.btn-edit');

                // console.log(btnEditCard);
                btnEditCard.setAttribute('data-name', 'edit-card');
                btnEditCard.setAttribute('data-target', '#editCardModal');
                btnEditCard.setAttribute('data-toggle', 'modal');

                btnEditCard.addEventListener('click', ()=>{
                    let lastModal=document.querySelector('.modal');

                    if(lastModal) {
                        lastModal.remove();
                    }
                    getModalId(btnEditCard);

                    let whatType=btnEditCard.dataset.name;
                    const createCardModal = new Modal (modalId, 'Edit Card');
                    createCardModal.createModal();
                    let innerForm = new Form (whatType);
                    console.log("Sending target ID" ,btnEditCard.id.slice(13));
                    innerForm.createForm(whatType, btnEditCard.id.slice(13));
                    innerForm.renderForm(document.querySelector('.modal-body'));
                });



                const btnDeleteCard = card.querySelector('.btn-remove');
                btnDeleteCard.setAttribute('data-name', 'delete-card');
                btnDeleteCard.setAttribute('data-target', '#deleteCardModal');
                btnDeleteCard.setAttribute('data-toggle', 'modal');

                btnDeleteCard.addEventListener('click', ()=>{
                    let lastModal=document.querySelector('.modal');

                    if(lastModal) {
                        lastModal.remove();
                    }
                    getModalId(btnDeleteCard);

                    let whatType=btnDeleteCard.dataset.name;
                    const createCardModal = new Modal (modalId, 'Delete Card');
                    createCardModal.createModal();
                    let innerForm = new Form (whatType);
                    innerForm.createForm(whatType, btnDeleteCard.id.slice(15));
                    innerForm.renderForm(document.querySelector('.modal-body'));
                });



            });


        }else {
            parent.innerHTML = `<div class="nocards-added-message-cover d-flex justify-content-center align-items-center">
            <h2 class="nocards-added-message">No items have been added.</h2></div>`;
        }
    }

    async updateCards(url) {
        if(!document.cookie){
            location.reload();
            return;
        }
        const getCardRequest = await axios.get(url, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${document.cookie}`
            }
        })
        return getCardRequest.data
       
    }
}

// const token = '608373175d22';
const target = document.querySelector('.main-container');


