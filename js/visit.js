class Visit {
    constructor(name, purpose, briefDescription, md, priority) {
        this.Name = name;
        this.Purpose = purpose;
        this.Brief_Description = briefDescription;
        this.MD = md;
        this.Priority = priority;
    }


    async updateCreateVisit(token, data, id) {
        if(!token){
            location.reload();
            return;
        }
        if (id) {
            const createRemoteVisit = await axios.put(`http://cards.danit.com.ua/cards/${id}`, data, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                }
            }).then(response => {
                if (!this.id) {
                    this.id = response.data.id;
                } else {
                    console.log('Error in card update. ID mismatch.');
                }
               board.renderCards(document.querySelector('.card-row'));
            });
        } else {
            console.log("ID wasn't set");
            const createRemoteVisit = await axios.post('http://cards.danit.com.ua/cards', data, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                }
            }).then(response => {
                this.id = response.data.id;
               board.renderCards(document.querySelector('.card-row'));//can't set await here. Why?
            });
        }
    }
}

class VisitCardiolodist extends Visit {
    constructor(name, purpose, briefDescription, md, priority, normalBloodPressure, bodyMassIndex, cardioVascularIlnesses, age, id) {
        super(name, purpose, briefDescription, md, priority);
        this.BP = normalBloodPressure;
        this.BMI = bodyMassIndex;
        this.Ilnesses = cardioVascularIlnesses;
        this.Age = age;
        super.updateCreateVisit(document.cookie, this, id);
    }
}

class VisitDentist extends Visit {
    constructor(name, purpose, briefDescription, md, priority, lastVisit, id) {
        super(name, purpose, briefDescription, md, priority);
        this.Last_Visit = lastVisit;
        super.updateCreateVisit(document.cookie, this, id);
    }
}

class VisitTherapist extends Visit {
    constructor(name, purpose, briefDescription, md, priority, age, id) {
        super(name, purpose, briefDescription, md, priority);
        this.Age = age;
        super.updateCreateVisit(document.cookie, this, id);
    }
}