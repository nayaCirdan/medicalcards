## Step Project Cards
# Условием корректной работы системы является запуск проекта через **liveserver.**
- Login - bondar.stan@gmail.com
- Password - f4ulKrZz6NuO


### Students
- Ruslan Makhortykh
- Stanislav Bondar
- Oksana Kirdan


#### Oksana Kirdan
1) Общий дизайн проекта
2) Класс модального окна `Modal` (Bootstrap + JS реализация)
3) Класс формы `Form` и методы ее логики
4) Фильтрация карточек + AJAX

#### Ruslan Makhortykh
1) Создание отдельного класса, для компонентов формы `CreatItemForForm`

#### Stanislav Bondar
1) Класс доски `MainBoard` 
2) Редактирование карт, удаление карт, создание карт (изменение, удаление и создание нового объекта карты AJAX)
3) Создание класса `Visit` и наследуемых `VisitDentist`, `VisitCardiologist`, `VisitTherapist`
4) Реализация авторизации с записью в куки
5) Drag&drop